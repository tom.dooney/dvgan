# Imports.
import tensorflow as tf
from tensorflow import keras
from keras import layers
from tensorflow.keras.optimizers import RMSprop

from training_data import *
from model_components import conv_block, upsample_block, get_discriminator_model, get_derivative_discriminator_model, get_generator_model
from gan_models import WGAN, DVGAN
from utils import discriminator_loss, generator_loss, calculate_derivative, plot_GAN_history, plot_examples, GANMonitor, fit_GAN

import pickle
import json
import os

# Set parameters.
sample_rate = 1024
n_signals = 6000
noise_dim = 100

# Set directories for storing outputs
output_dir = 'GAN_outputs/'
monitor_dir = 'Monitor/'

dataset_choice_dict = {'1':'pulse',
                      '2':'ringdown',
                      '3':'bbh'}

gan_choice_dict = {'1':'WGAN',
                  '2':'DVGAN'}

# Create the dataset. Options are gaussianblip, ringdown or bbhinspiral
# which take the same sample_rate and n_signals parameters.
dataset_choice_int = input('Please input dataset choice (1: pulse, 2: ringdown, 3: bbh): ')
gan_choice_int = input('Please input the GAN variant to train (1: WGAN, 2: DVGAN): ')

dataset_choice = dataset_choice_dict[dataset_choice_int]
gan_choice = gan_choice_dict[gan_choice_int]

if dataset_choice == 'pulse':
    data = gaussianpulse(sample_rate,n_signals)
elif dataset_choice == 'ringdown':
    data = ringdown(sample_rate,n_signals)
elif dataset_choice == 'bbh':
    print('BBH simulations can take some time, please wait...')
    data = bbhinspiral(sample_rate,n_signals)
else:
    raise ValueError('Please choose between pulse (1), ringdown (2) or bbh (3)')
data = np.array(data)

signal_length = data.shape[1]

# Calculate the derivative signals of the training data.
data_deriv = calculate_derivative(np.array(range(signal_length)).astype('float32'), data.astype('float32'))


# Defining the model components, print_summary=True shows the model component summaries.
d_model = get_discriminator_model(print_summary=True)
g_model = get_generator_model(print_summary=True)

# Instantiating the optimizers.
generator_optimizer = RMSprop(.0001)
discriminator_optimizer = RMSprop(.0001)

if gan_choice == 'WGAN':
    # Instantiate the WGAN model.
    gan = WGAN(
        discriminator=d_model,
        generator=g_model,
        latent_dim=noise_dim,
        discriminator_extra_steps=5,
    )

    # Compile the WGAN model.
    gan.compile(
        d_optimizer=discriminator_optimizer,
        g_optimizer=generator_optimizer,
        d_loss_fn=discriminator_loss,
        g_loss_fn=generator_loss
    )
elif gan_choice == 'DVGAN':
    d_model_derivatives = get_derivative_discriminator_model(print_summary=True)
    # Instantiate the DVGAN model.
    gan = DVGAN(
        discriminator=d_model,
        deriv_discriminator = d_model_derivatives,
        generator=g_model,
        latent_dim=noise_dim,
        discriminator_extra_steps=5,
    )

    # Compile the DVGAN model.
    gan.compile(
        d_optimizer=discriminator_optimizer,
        d2d_optimizer=discriminator_optimizer,
        g_optimizer=generator_optimizer,
        d_loss_fn=discriminator_loss,
        g_loss_fn=generator_loss
    )
else:
    raise ValueError('Please choose between WGAN (1) or DVGAN (2)')


# Set batch size and number of epochs for training.
BATCH_SIZE = 512
epochs = 500

# Change to False if you don't want the GAN monitor to plot generated signals after each epoch.
callback = False
# Callback path to save GAN monitor signals.
callback_path = monitor_dir+gan_choice+'/'+dataset_choice

# Start training the model, saving the histroy information.
history = fit_GAN(gan, data, data_deriv, batch_size=BATCH_SIZE, epochs=epochs, gan_variant = gan_choice, callback = callback, noise_dim = noise_dim, callback_path=callback_path)

# Output path where loss plots, generated examples and trained generators are stored.
output_path = output_dir+gan_choice+'/'+dataset_choice
# output_path = 'DVGAN_outputs_aux/'+dataset_choice
isExist = os.path.exists(output_path)
if not isExist:
    os.makedirs(output_path)

# Plot training history.
plot_GAN_history(history, output_path, gan_choice)

# Save history as json.
# Get the dictionary containing each metric and the loss for each epoch.
history_dict = history.history
# Dump it
json.dump(history_dict, open(output_path+'/history.json', 'w'))

# Save the generator.
gan.generator.save(output_path+'/Generator')

# Generate signals.
num_signals = 6000
latent_vectors = tf.random.normal(shape=(num_signals, noise_dim))
generations = gan.generator(latent_vectors)
generations = generations.numpy()

# Plot some examples of the data and generated samples for a glance comparison.
plot_examples(data, output_path+'/Data_examples')
plot_examples(generations, output_path+'/Generation_examples')

# Save pickle files of training and generated data.
with open(output_path+'/data.pkl','wb') as f: pickle.dump(data, f)
with open(output_path+'/generations.pkl','wb') as f: pickle.dump(generations, f)
