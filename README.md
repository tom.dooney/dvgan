# DVGAN

DVGAN is a three-player 1-D Wasserstein Generative Adversarial Network that uses an additional discriminator to distinguish between real and fake derivative signals. It is applied to continuous time series data, in the setting of Gravitational Wave (GW) physics. The user may also decide to train a vanilla two-player Wasserstein GAN rather than DVGAN. Three different proxy GW waveform datasets may be selected from to train a GAN model. The associated paper can be read [here](https://arxiv.org/abs/2209.13592).

## Requirements

The current version is working on Python 3.9.7 and Tensorflow 2.8.0. PyCBC (working on 1.18.3) also required for simulating BBH dataset. The use of a GPU is recommended.

## Usage

Clone the respository to your directory of choice. The driver file is `train_gan.py`, where various parameters can be changed, such as sample rate, the number of signals to model (and generate after training) and the number of training epochs. 

If you would like to change the name of output directories, the following variables can be changed:

```
# Set directories for storing outputs
output_dir = 'GAN_outputs/'
monitor_dir = 'Monitor/'
```

The model can be trained from the command line by running `python train_gan.py`. The user is prompted to input a dataset to train the GAN on, the options including 'pulse', 'ringdown' or 'bbh'. The bbh dataset is simulated from PyCBC's `get_td_waveform` function, and can take some time to finish generating. The user is then prompted to enter whether they want to train a 'WGAN' or a 'DVGAN'. After this, the chosen GAN model will be trained on the chosen dataset for the number of epochs specified in `train_gan.py`. Directories named according to the GAN model and dataset used will be created within the output directory, where outputs including the GAN generator, training data, GAN generations, plots of generated example and loss plots are saved.

