import numpy as np
import tensorflow as tf
import keras
from utils import calculate_derivative


class WGAN(keras.Model):
    def __init__(
        self,
        discriminator,
        generator,
        latent_dim,
        discriminator_extra_steps=5,
        gp_weight=10.0,
    ):
        super(WGAN, self).__init__()
        self.discriminator = discriminator
        self.generator = generator
        self.latent_dim = latent_dim
        self.d_steps = discriminator_extra_steps
        self.gp_weight = gp_weight

    def compile(self, d_optimizer, g_optimizer, d_loss_fn, g_loss_fn):
        super(WGAN, self).compile()
        self.d_optimizer = d_optimizer
        self.g_optimizer = g_optimizer
        self.d_loss_fn = d_loss_fn
        self.g_loss_fn = g_loss_fn

    def gradient_penalty(self, batch_size, real_signals, fake_signals):
        """ Calculates the gradient penalty.

        This loss is calculated on an interpolated signal
        and added to the discriminator loss.
        """
        # Get the interpolated signal
        alpha = tf.random.normal([batch_size, 1], 0.0, 1.0, dtype=tf.float32)
        diff = fake_signals - real_signals
        interpolated = real_signals + alpha * diff

        with tf.GradientTape() as gp_tape:
            gp_tape.watch(interpolated)
            # 1. Get the discriminator output for this interpolated signal.
            pred = self.discriminator(interpolated, training=True)

        # 2. Calculate the gradients w.r.t to this interpolated signal.
        grads = gp_tape.gradient(pred, [interpolated])[0]
        # 3. Calculate the norm of the gradients.
        norm = tf.sqrt(tf.reduce_sum(tf.square(grads), axis=None))
        gp = tf.reduce_mean((norm - 1.0) ** 2)
        return gp

    def train_step(self, data):
        real_signals = tf.cast(data, dtype = tf.float32)
        # Get the batch size
        batch_size = tf.shape(real_signals)[0]
        
        # For each batch, we are going to perform the
        # following steps as laid out in the original paper:
        # 1. Train the generator and get the generator loss
        # 2. Train the discriminator and get the discriminator loss
        # 3. Calculate the gradient penalty
        # 4. Multiply this gradient penalty with a constant weight factor
        # 5. Add the gradient penalty to the discriminator loss
        # 6. Return the generator and discriminator losses as a loss dictionary

        # Train the discriminator first. The original paper recommends training
        # the discriminator for `x` more steps (typically 5) as compared to
        # one step of the generator. Here we will train it for 3 extra steps
        # as compared to 5 to reduce the training time.
        for i in range(self.d_steps):
            # Get the latent vector
            random_latent_vectors = tf.random.normal(
                shape=(batch_size, self.latent_dim)
            )
            with tf.GradientTape() as tape:
                # Generate fake signals from the latent vector
                fake_signals = self.generator(random_latent_vectors, training=True)
                # Get the logits for the fake signals
                fake_logits = self.discriminator(fake_signals, training=True)
                # Get the logits for the real signals
                real_logits = self.discriminator(real_signals, training=True)

                # Calculate the discriminator loss using the fake and real image logits
                d_cost = self.d_loss_fn(real_sig=real_logits, fake_sig=fake_logits)
                # Calculate the gradient penalty
                gp = self.gradient_penalty(batch_size, real_signals, fake_signals)
                # Add the gradient penalty to the original discriminator loss
                d_loss = d_cost + gp * self.gp_weight

            # Get the gradients w.r.t the discriminator loss
            d_gradient = tape.gradient(d_loss, self.discriminator.trainable_variables)
            # Update the weights of the discriminator using the discriminator optimizer
            self.d_optimizer.apply_gradients(
                zip(d_gradient, self.discriminator.trainable_variables)
            )

        # Train the generator
        # Get the latent vector
        random_latent_vectors = tf.random.normal(shape=(batch_size, self.latent_dim))
        with tf.GradientTape() as tape:
            # Generate fake signals using the generator
            generated_signals= self.generator(random_latent_vectors, training=True)
            # Get the discriminator logits for fake signals
            gen_sig_logits = self.discriminator(generated_signals, training=True)
            # Calculate the generator loss
            g_loss = self.g_loss_fn(gen_sig_logits)

        # Get the gradients w.r.t the generator loss
        gen_gradient = tape.gradient(g_loss, self.generator.trainable_variables)
        # Update the weights of the generator using the generator optimizer
        self.g_optimizer.apply_gradients(
            zip(gen_gradient, self.generator.trainable_variables)
        )
        return {"d_loss": d_loss, "g_loss": g_loss}

class DVGAN(keras.Model):
    def __init__(
        self,
        discriminator,
        deriv_discriminator,
        generator,
        latent_dim,
        discriminator_extra_steps=5,
        gp_weight=10.0,
    ):
        super(DVGAN, self).__init__()
        self.discriminator = discriminator
        self.deriv_discriminator = deriv_discriminator
        self.generator = generator
        self.latent_dim = latent_dim
        self.d_steps = discriminator_extra_steps
        self.gp_weight = gp_weight

    def compile(self, d_optimizer, d2d_optimizer, g_optimizer, d_loss_fn, g_loss_fn):
        super(DVGAN, self).compile()
        self.d_optimizer = d_optimizer
        self.d2d_optimizer = d2d_optimizer
        self.g_optimizer = g_optimizer
        self.d_loss_fn = d_loss_fn
        self.d2d_loss_fn = d_loss_fn
        self.g_loss_fn = g_loss_fn

    def gradient_penalty(self, batch_size, real_signals, fake_signals):
        """ Calculates the gradient penalty.

        This loss is calculated on an interpolated signal
        and added to the discriminator loss.
        """
        # Get the interpolated signal
        alpha = tf.random.normal([batch_size, 1], 0.0, 1.0, dtype=tf.float32)
        diff = fake_signals - real_signals
        interpolated = real_signals + alpha * diff

        with tf.GradientTape() as gp_tape:
            gp_tape.watch(interpolated)
            # 1. Get the discriminator output for this interpolated signal.
            pred = self.discriminator(interpolated, training=True)

        # 2. Calculate the gradients w.r.t to this interpolated signal.
        grads = gp_tape.gradient(pred, [interpolated])[0]
        # 3. Calculate the norm of the gradients.
        norm = tf.sqrt(tf.reduce_sum(tf.square(grads), axis=None))
        gp = tf.reduce_mean((norm - 1.0) ** 2)
        return gp

    def gradient_penalty_derivative(self, batch_size, real_signals, fake_signals):
        """ Calculates the gradient penalty.

        This loss is calculated on an interpolated derivative signal
        and added to the derivative discriminator loss.
        """
        # Get the interpolated signal
        alpha = tf.random.normal([batch_size, 1], 0.0, 1.0, dtype=tf.float32)
        diff = fake_signals - real_signals
        interpolated = real_signals + alpha * diff

        with tf.GradientTape() as gp_tape:
            gp_tape.watch(interpolated)
            # 1. Get the discriminator output for this interpolated signal.
            pred = self.deriv_discriminator(interpolated, training=True)

        # 2. Calculate the gradients w.r.t to this interpolated signal.
        grads = gp_tape.gradient(pred, [interpolated])[0]
        # 3. Calculate the norm of the gradients.
        norm = tf.sqrt(tf.reduce_sum(tf.square(grads), axis=None))
        gp = tf.reduce_mean((norm - 1.0) ** 2)
        return gp

    def train_step(self, data):
        real_signals = tf.cast(data[0][0], dtype = tf.float32)
        real_derivatives= tf.cast(data[0][1], dtype = tf.float32)
        
        # Get the batch size
        batch_size = tf.shape(real_signals)[0]

        # For each batch, we are going to perform the
        # following steps as laid out in the original paper:
        # 1. Train the generator and get the generator loss
        # 2. Train the discriminators and get the discriminator losses
        # 3. Calculate the gradient penalties
        # 4. Multiply this gradient penalties with a constant weight factor
        # 5. Add the respective gradient penalties to each discriminator loss
        # 6. Return the generator and discriminator losses as a loss dictionary

        # Train the discriminator first.
        for i in range(self.d_steps):
            # Get the latent vector
            random_latent_vectors = tf.random.normal(
                shape=(batch_size, self.latent_dim)
            )
            with tf.GradientTape(persistent=True) as tape:
                # Generate fake signals from the latent vector
                fake_signals = self.generator(random_latent_vectors, training=True)
                # fake_signals = tf.cast(fake_signals, dtype=tf.float32)
                
                fake_derivatives = calculate_derivative(tf.cast(np.array(range(1024)), tf.float32), fake_signals)     
                # Get the logits for the fake signals
                fake_logits = self.discriminator(fake_signals, training=True)
                # Get the logits for the real signals
                real_logits = self.discriminator(real_signals, training=True)

                # Get the logits for the fake derivatives
                fake_logits2d = self.deriv_discriminator(fake_derivatives, training=True)
                # Get the logits for the real derivatives
                real_logits2d = self.deriv_discriminator(real_derivatives, training=True)

                # Calculate the discriminator loss using the fake and real signal logits
                d_cost = self.d_loss_fn(real_sig=real_logits, fake_sig=fake_logits)

                # Calculate the derivative discriminator loss using the fake and real signal logits
                d2d_cost = self.d_loss_fn(real_sig=real_logits2d, fake_sig=fake_logits2d)

                # Calculate the gradient penalty
                gp = self.gradient_penalty(batch_size, real_signals, fake_signals)

                # Calculate the gradient penalty
                gp2d = self.gradient_penalty_derivative(batch_size, real_derivatives, fake_derivatives)

                # Add the gradient penalty to the original discriminator loss                
                d_loss = d_cost + gp * self.gp_weight
                d2d_loss = d2d_cost + gp2d * self.gp_weight

            # Get the gradients w.r.t the discriminator loss
            d_gradient = tape.gradient(d_loss, self.discriminator.trainable_variables)

            # Get the gradients w.r.t the deriv_discriminator loss
            d2d_gradient = tape.gradient(d2d_loss, self.deriv_discriminator.trainable_variables)

            # Update the weights of the discriminator using the discriminator optimizer
            self.d_optimizer.apply_gradients(
                zip(d_gradient, self.discriminator.trainable_variables)
            )

            # Update the weights of the deriv_discriminator using the deriv_discriminator optimizer
            self.d2d_optimizer.apply_gradients(
                zip(d2d_gradient, self.deriv_discriminator.trainable_variables)
            )

        # Train the generator
        # Get the latent vector
        random_latent_vectors = tf.random.normal(shape=(batch_size, self.latent_dim))
        with tf.GradientTape() as tape:
            # Generate fake signals using the generator
            generated_signals = self.generator(random_latent_vectors, training=True)

            # Calculate derivatives from fake signals
            generated_derivatives = calculate_derivative(tf.cast(np.array(range(1024)), tf.float32), generated_signals)

            # Get the discriminator logits for fake signals
            gen_sig_logits = self.discriminator(generated_signals, training=True)
            # Get the deriv_discriminator logits for fake derivatives
            gen_sig2d_logits = self.deriv_discriminator(generated_derivatives, training=True)
            # Calculate the generator loss
            g_loss = self.g_loss_fn(gen_sig_logits)

            g_loss2d = self.g_loss_fn(gen_sig2d_logits)

            g_loss_combined = (1/2)*(g_loss+g_loss2d)

        # Get the gradients w.r.t the generator loss
        gen_gradient = tape.gradient(g_loss_combined, self.generator.trainable_variables)
        # Update the weights of the generator using the generator optimizer
        self.g_optimizer.apply_gradients(
            zip(gen_gradient, self.generator.trainable_variables)
        )
        return {"d_loss": d_loss, "d2d_loss": d2d_loss, "g_loss": g_loss, "g_loss2d":g_loss2d, "g_loss_combined":g_loss_combined}
