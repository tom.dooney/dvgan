import keras
from keras import layers

def conv_block(
    x,
    filters,
    activation=layers.LeakyReLU(),
    kernel_size=5,
    strides=1,
    padding="same",
    use_bias=True,
    use_bn=False,
    use_dropout=False,
    drop_value=0.5,
):
    x = layers.Conv1D(
        filters, kernel_size, strides=strides, padding=padding, use_bias=use_bias
    )(x)
    if use_bn:
        x = layers.BatchNormalization()(x)
    x = activation(x)
    if use_dropout:
        x = layers.Dropout(drop_value)(x)
    return x


def get_discriminator_model(print_summary = False):
    sig_input = layers.Input(shape=(1024,))
    x = layers.Reshape((1024,1))(sig_input)
    x = conv_block(
        x,
        64,
        activation=layers.LeakyReLU(),
        strides=2,
        use_bn=False,
        use_bias=True,
        use_dropout=False,
        drop_value=0.5,
    )
    x = conv_block(
        x,
        128,
        activation=layers.LeakyReLU(),
        strides=2,
        use_bn=False,
        use_bias=True,
        use_dropout=True,
        drop_value=0.5,
    )
    x = conv_block(
        x,
        256,
        activation=layers.LeakyReLU(),
        strides=2,
        use_bn=False,
        use_bias=True,
        use_dropout=True,
        drop_value=0.5,
    )
    x = conv_block(
        x,
        512,
        activation=layers.LeakyReLU(),
        strides=2,
        use_bn=False,
        use_bias=True,
        use_dropout=False,
        drop_value=0.5,
    )

    x = layers.Flatten()(x)
    x = layers.Dense(100)(x)
    x = layers.LeakyReLU()(x)
    x = layers.Dropout(0.2)(x)
    x = layers.Dense(1, activation='sigmoid')(x)

    d_model = keras.models.Model(sig_input, x, name="discriminator")
    if print_summary:
        print(d_model.summary())
    return d_model


def get_derivative_discriminator_model(print_summary = False):
    sig_input = layers.Input(shape=(1023,))
    x = layers.Reshape((1023,1))(sig_input)
    x = conv_block(
        x,
        32,
        activation=layers.LeakyReLU(),
        strides=2,
        use_bn=False,
        use_bias=True,
        use_dropout=False,
        drop_value=0.5,
    )
    x = conv_block(
        x,
        64,
        activation=layers.LeakyReLU(),
        strides=2,
        use_bn=False,
        use_bias=True,
        use_dropout=True,
        drop_value=0.5,
    )
    x = conv_block(
        x,
        128,
        activation=layers.LeakyReLU(),
        strides=2,
        use_bn=False,
        use_bias=True,
        use_dropout=True,
        drop_value=0.5,
    )


    x = layers.Flatten()(x)
    x = layers.Dense(1, activation='sigmoid')(x)

    d_model = keras.models.Model(sig_input, x, name="derivative_discriminator")
    if print_summary:
        print(d_model.summary())
    return d_model


def upsample_block(
    x,
    filters,
    activation = layers.ReLU(),
    kernel_size=5,
    strides=2,
    up_size=2,
    padding="same",
    use_bn=False,
    use_bias=True,
    use_dropout=False,
    drop_value=0.3,
):
    x = layers.UpSampling1D(up_size)(x)
    x = layers.Conv1D(
        filters, kernel_size, strides=strides, padding=padding, use_bias=use_bias
    )(x)

    if use_bn:
        x = layers.BatchNormalization()(x)

    if activation:
        x = activation(x)
    if use_dropout:
        x = layers.Dropout(drop_value)(x)
    return x


noise_dim = 100
 
def get_generator_model(print_summary=False):
    noise = layers.Input(shape=(noise_dim,))
    x = layers.Dense(32768, use_bias=False)(noise)
    x = layers.ReLU()(x)

    x = layers.Reshape((64, 512))(x)
    x = upsample_block(
        x,
        256,
        activation = layers.ReLU(),
        strides=1,
        use_bias=False,
        use_bn=True,
        padding="same",
        use_dropout=False,
    )
    x = upsample_block(
        x,
        128,
        activation = layers.ReLU(),
        strides=1,
        use_bias=False,
        use_bn=False,
        padding="same",
        use_dropout=False,
    )
    x = upsample_block(
        x,
        64,
        activation = layers.ReLU(),
        strides=1,
        use_bias=False,
        use_bn=False,
        padding="same",
        use_dropout=False,
    )

    x = upsample_block(
        x,
        1,
        activation = False,
        strides=1,
        use_bias=False,
        use_bn=False,
        padding="same",
        use_dropout=False,
    )

    x = layers.Reshape((1024,))(x)

    g_model = keras.models.Model(noise, x, name="generator")
    if print_summary:
        print(g_model.summary())
    return g_model
