import tensorflow as tf
import matplotlib.pyplot as plt
import keras
from keras import backend as K
import os

# Define the loss function for the discriminators,
# which should be (fake_loss - real_loss).
# We will add the gradient penalty later to this loss function.
def discriminator_loss(real_sig, fake_sig):
    """Calculate the Wasserstein Loss for discriminators.
    
    Parameters
    ---------

    real_sig: class: 'tf.Tensor'
            logits for real signals
            
    fake_sig: class: 'tf.Tensor'
            logits for fake signals
            
    Returns
    ---------
    
    Discriminator Wasserstein loss
    """
    real_loss = tf.reduce_mean(real_sig)
    fake_loss = tf.reduce_mean(fake_sig)
    return fake_loss - real_loss


# Define the loss function for the generator.
def generator_loss(fake_sig):
    """Calculate the Wasserstein Loss for generator.
    
    Parameters
    ---------     
    
    fake_sig: class: 'tf.Tensor'
            logits for fake signals
            
    Returns
    ---------
    
    Generator Wasserstein loss
    """
    return -tf.reduce_mean(fake_sig)

@tf.function
def calculate_derivative(x, y):
    """A tf function that calculates the derivative of one vector with respect to another.
    Used to calculate derivative signals of generated samples during training."""
    dydx = tf.experimental.numpy.diff(y)/tf.experimental.numpy.diff(x)
    return dydx

    
def plot_GAN_history(history, path, gan_variant):
    """A function to plot and save the WGAN or DVGAN losses to a certain path."""
    if gan_variant =='WGAN':
        plt.figure()
        plt.plot(history.history['d_loss'], 'b-', label = 'Discriminator')
        plt.plot(history.history['g_loss'], 'm-', label = 'Generator')
        plt.legend()
        plt.savefig(path+'/WGAN_loss_plot')
        plt.close()
    else:
        plt.figure()
        plt.plot(history.history['d_loss'], 'b-', label = 'Discriminator 1')
        plt.plot(history.history['d2d_loss'], 'r-', label = 'Discriminator 2')
        plt.plot(history.history['g_loss'], 'g-', label = 'Generator 1')
        plt.plot(history.history['g_loss2d'], 'c-', label = 'Generator 2')
        plt.plot(history.history['g_loss_combined'], 'm-', label = 'Generator Combined')
        plt.legend()
        plt.savefig(path+'/DVGAN_loss_plot')
        plt.close()

def plot_examples(data, path):
    """A function to plot and save 9 examples of training or generated data."""
    plt.figure(figsize=(12,7))
    for i in range(9):
        plt.subplot(3, 3, i+1)
        plt.plot(data[i])

    plt.savefig(path)
    plt.close()

    
def fit_GAN(GAN, data, data_deriv, batch_size, epochs, gan_variant = 'DVGAN', callback = False, noise_dim = 100, callback_path = 'DVGAN_monitor'):
    """A function to train the GAN model. We can choose to pass call backs to monitor the training after each epoch.
    
    Parameters
    ---------
    
    GAN: class: 'keras.Model'
            The GAN model to fit to data

    data: class:'numpy.ndarray' or 'tf.Tensor'
            A 1D array holding signals
            
    data_deriv: class:'numpy.ndarray' or 'tf.Tensor'
            A 1D array holding derivative signals
            
    batch_size: int
            The batch size for the model
        
    epochs: int
            The number of epochs to train the model
            
    gan_variant: str
            The variant of the GAN to be trained, either 'WGAN' or 'DVGAN'
          
    callback: bool
            Boolean controlling whether GAN monitor should be used
    
    callback_path: str
            The path where GAN monitor images are saved during training
            
    """
    
    if gan_variant == 'DVGAN':
        data = [data, data_deriv]
    if callback:
        # Instantiate the 'GANMonitor' for Keras callbacks.
        cbk = GANMonitor(num_img=1, latent_dim=noise_dim, callback_path=callback_path)
        isExist = os.path.exists(callback_path)
        if not isExist:
            os.makedirs(callback_path)
        
        history = GAN.fit(data, batch_size=batch_size, epochs=epochs, callbacks = [cbk])
    else:
        history = GAN.fit(data, batch_size=batch_size, epochs=epochs)
        
    return history
    
    
class GANMonitor(keras.callbacks.Callback):
    """GAN monitor used to plot GAN generated data after each epoch."""
    def __init__(self, num_img=1, latent_dim=100, callback_path = 'DVGAN_monitor'):
        self.num_img = num_img
        self.latent_dim = latent_dim
        self.callback_path = callback_path

    def on_epoch_end(self, epoch, logs=None):
        random_latent_vectors = tf.random.normal(shape=(self.num_img, self.latent_dim))
        generated_signals = self.model.generator(random_latent_vectors)        
        

        for i in range(self.num_img):
            img = generated_signals[i].numpy()
            plt.figure()
            plt.plot(img)
            plt.savefig(self.callback_path+"/generated_img_{i}_{epoch}.png".format(i=i, epoch=epoch),format="png")
            plt.close()
            
            

